const router = require('express').Router()
const Admin = require('../models/Admin')
const User = require('../models/User')
const { check, validationResult } = require('express-validator')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs')
const config = require('config')
const adminAuth = require('../middleware/adminAuth')

// Register User
router.post(
  '/new',
  [
    [adminAuth],
    check('firstName', 'First Name is required').not().isEmpty(),
    check('lastName', 'Last Name is required').not().isEmpty(),
    check('email', 'Email Address is required').isEmail(),
    check('password', 'password is required').isLength({ min: 6 }),
  ],
  async (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() })
    }

    const { firstName, lastName, email, password } = req.body
    try {
      let admin = await Admin.findOne({ email })

      if (admin) {
        return res
          .status(400)
          .json({ message: 'Admin with this email address already exists' })
      }

      admin = new Admin({
        firstName,
        lastName,
        email,
        password,
      })

      const salt = await bcrypt.genSalt(10)

      admin.password = await bcrypt.hash(password, salt)

      await admin.save()

      const payload = {
        admin: {
          id: admin.id,
        },
      }

      jwt.sign(
        payload,
        config.get('jwtsecret'),
        { expiresIn: 36000 },
        (err, token) => {
          if (err) throw err
          res.json({ token })
        }
      )
    } catch (error) {
      console.error(error.message)
      res.status(500).send('Server error')
    }
  }
)

// Login Admin Account
router.post(
  '/auth',
  [
    check('email', 'Email address is required').not().isEmpty().isEmail(),
    check('password', 'Password is required')
      .not()
      .isEmpty()
      .isLength({ min: 6 }),
  ],
  async (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() })
    }

    const { email, password } = req.body

    try {
      const admin = await Admin.findOne({ email })
      if (!admin) {
        return res.status(400).json({ message: 'Invalid Credentials' })
      }

      const passwordMatch = await bcrypt.compare(password, admin.password)

      if (!passwordMatch) {
        return res.status(400).json({ message: 'Invalid Credentials' })
      }

      const payload = {
        admin: {
          id: admin.id,
        },
      }

      jwt.sign(
        payload,
        config.get('jwtsecret'),
        { expiresIn: 3600 },
        (err, token) => {
          if (err) throw err
          res.json({ token })
        }
      )
    } catch (error) {
      console.error(error.message)
      res.status(500).send('Server Error')
    }
  }
)

// Authenticate Token
router.get('/', adminAuth, async (req, res) => {
  try {
    const admin = await Admin.findById(req.admin.id).select('-password')
    res.json(admin)
  } catch (error) {
    console.error(error.message)
    res.status(500).send('Server Error')
  }
})

// Get all admin accounts
router.get('/all', adminAuth, async (req, res) => {
  try {
    const admins = await Admin.find().select('-password').sort({ date: -1 })
    res.json(admins)
  } catch (error) {
    console.error(error.message)
    res.status(500).send('Server Error')
  }
})

// Deactivate Admin Privilages
router.put('/status/:id', adminAuth, async (req, res) => {
  const { status } = req.body
  try {
    const currentAdmin = await Admin.findById(req.admin.id)
    let admin = await Admin.findById(req.params.id)

    if (
      currentAdmin.accountType !== 'Admin' ||
      currentAdmin.accountStatus !== 'Active'
    ) {
      return res
        .status(401)
        .json({ message: 'Action Denied! You do not have admin previlages.' })
    }

    if (!admin) return res.status(404).json({ message: 'Admin not found' })
    const statusField = {
      accountStatus: status,
    }

    admin = await Admin.findByIdAndUpdate(
      req.params.id,
      { $set: statusField },
      { new: true }
    )

    res.json(admin)
  } catch (error) {
    console.error(error.message)
    res.status(500).send('Server Error')
  }
})

// Delete Admin Account
router.delete('/remove/:id', adminAuth, async (req, res) => {
  try {
    const currentAdmin = await Admin.findById(req.admin.id)
    let admin = await Admin.findById(req.params.id)

    if (
      currentAdmin.accountStatus !== 'Active' ||
      currentAdmin.accountType !== 'Admin'
    ) {
      return res.status(401).json({
        message: 'You do not have the admin previlages for this action.',
      })
    }

    if (!admin) {
      return res.status(404).json({ message: 'Admin account not found' })
    }

    admin = await Admin.findByIdAndDelete(req.params.id)

    res.json({ message: 'Admin Deleted Successfully' })
  } catch (error) {
    console.error(error.message)
    res.status(500).send('Server Error')
  }
})

// Change Admin Account Type
router.put('/type/:id', adminAuth, async (req, res) => {
  const { type } = req.body
  try {
    const currentAdmin = await Admin.findById(req.admin.id)
    let admin = await Admin.findById(req.params.id)

    if (
      currentAdmin.accountType !== 'Admin' ||
      currentAdmin.accountStatus !== 'Active'
    ) {
      return res
        .status(401)
        .json({ message: 'Action Denied! You do not have admin previlages.' })
    }

    if (!admin) return res.status(404).json({ message: 'Admin not found' })
    const typeField = {
      accountType: type,
    }

    admin = await Admin.findByIdAndUpdate(
      req.params.id,
      { $set: typeField },
      { new: true }
    )

    res.json(admin)
  } catch (error) {
    console.error(error.message)
    res.status(500).send('Server Error')
  }
})

// DELETE USERS AND VERIRY SUBSCRIPTION

// Delete Users
router.delete('/users/:id', adminAuth, async (req, res) => {
  try {
    let user = await User.findOne({ _id: req.params.id })
    const admin = await Admin.findOne({ _id: req.admin.id })
    if (!user) return res.status(404).json({ message: 'User not found' })

    if (admin.accountStatus !== 'Active')
      return res.status(401).json({
        message:
          'Oops, looks like your admin previlages have been revoked! Contact Database Admin.',
      })

    await user.deleteOne()

    res.json({ message: 'User Deleted!' })
  } catch (error) {
    console.error(error.message)
    res.status(500).send('Server Error')
  }
})

// Change Subscription
router.put('/users/:id', adminAuth, async (req, res) => {
  const { status } = req.body
  try {
    let user = await User.findById(req.params.id)
    if (!user) return res.status(404).json({ message: 'User not found' })

    // Build User Status
    const statusField = {
      accountStatus: status,
    }

    user = await User.findByIdAndUpdate(
      req.params.id,
      { $set: statusField },
      { new: true }
    )

    res.json(user)
  } catch (error) {
    console.error(error.message)
    res.status(500).send('Server Error')
  }
})

// Get all Users
router.get('/users', async (req, res) => {
  try {
    const users = await User.find().select('-password')
    res.json(users)
  } catch (error) {
    console.error(error.message)
    res.status(500).send('Server Error')
  }
})

module.exports = router
