const router = require('express').Router()
const User = require('../models/User')
const userAuth = require('../middleware/userAuth')
const { check, validationResult } = require('express-validator')
const bcrypt = require('bcryptjs')
const config = require('config')
const jwt = require('jsonwebtoken')

// Register User
router.post(
  '/new',
  [
    check('firstName', 'First Name is required').not().isEmpty(),
    check('lastName', 'Last Name is required').not().isEmpty(),
    check('email', 'Email address is required').not().isEmpty().isEmail(),
    check('password', 'Password is required')
      .not()
      .isEmpty()
      .isLength({ min: 6 }),
  ],
  async (req, res) => {
    const errors = validationResult(res)
    if (!errors.isEmpty()) {
      return res.status(500).json({ errors: errors.array() })
    }
    const { firstName, lastName, email, password } = req.body
    try {
      let user = await User.findOne({ email })

      if (user) {
        return res
          .status(400)
          .json({ message: 'A user with this email address already exists' })
      }

      user = new User({
        firstName,
        lastName,
        email,
        password,
      })

      const salt = await bcrypt.genSalt(10)

      user.password = await bcrypt.hash(password, salt)

      await user.save()

      const payload = {
        user: {
          id: user.id,
        },
      }

      jwt.sign(
        payload,
        config.get('jwtsecret'),
        { expiresIn: 36000 },
        (err, token) => {
          if (err) throw err
          res.json({ token })
        }
      )
    } catch (error) {
      console.error(error.message)
      res.status(500).send('Server Error')
    }
  }
)

// Login User
router.post(
  '/auth',
  [
    check('email', 'Email address is required').isEmpty().not().isEmpty(),
    check('password', 'Password is required')
      .isLength({ min: 6 })
      .not()
      .isEmpty(),
  ],
  async (req, res) => {
    const errors = validationResult(res)
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() })
    }

    const { email, password } = req.body

    try {
      const user = await User.findOne({ email })

      if (!user) {
        return res.status(400).json({ message: 'Invalid Credentials' })
      }

      const passwordMatched = await bcrypt.compare(password, user.password)

      if (!passwordMatched) {
        res.status(400).json({ message: 'Invalid Credentials' })
      }

      const payload = {
        user: {
          id: user.id,
        },
      }

      jwt.sign(
        payload,
        config.get('jwtsecret'),
        { expiresIn: 36000 },
        (err, token) => {
          if (err) throw err
          res.json({ token })
        }
      )
    } catch (error) {
      console.error(error.message)
      res.status(500).send('Server Error')
    }
  }
)

// Authenticate Token
router.get('/', userAuth, async (req, res) => {
  try {
    const user = await User.findById(req.user.id).select('-password')

    res.json(user)
  } catch (error) {
    console.error(error.message)
    res.status(500).send('Server Error')
  }
})

module.exports = router
