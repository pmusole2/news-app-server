const router = require('express').Router()
const adminAuth = require('../middleware/adminAuth')
const Admin = require('../models/Admin')
const User = require('../models/User')
const userAuth = require('../middleware/userAuth')
const { check, validationResult } = require('express-validator')
const Article = require('../models/Article')

// Get All Articles
// Admins Only
// Protected Route
router.get('/', adminAuth, async (req, res) => {
  try {
    const articles = await Article.find().sort({ postedAt: -1 })

    if (!articles) {
      return res
        .status(404)
        .json({ message: 'There are currently no articles' })
    }

    res.json(articles)
  } catch (error) {
    console.error(error.message)
    res.status(500).send('Server Error')
  }
})

// Create a new Article
// Protected Route
// Admins only
router.post(
  '/new',
  [
    adminAuth,
    [
      check('title', 'Article title is required').not().isEmpty(),
      check('body', 'Article Body is required').not().isEmpty(),
    ],
  ],
  async (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() })
    }

    const { title, body, articleType, category } = req.body

    try {
      const admin = await Admin.findById(req.admin.id)
      if (
        !admin ||
        admin.accountType !== 'Admin' ||
        admin.accountStatus !== 'Active'
      ) {
        return res.status(500).json({
          message:
            'Server Error, Please contact the Technical Department about your Account',
        })
      }

      const article = new Article({
        title,
        body,
        category,
        articleType: articleType && articleType,
        admin: {
          name: `${admin.firstName} ${admin.lastName}`,
          email: admin.email,
          id: admin._id,
        },
      })

      await article.save()

      res.json(article)
    } catch (error) {
      console.error(error.message)
      res.status(500).send('Server Error')
    }
  }
)

// Delete Article
// Admins Only
// Only Admin who created Artilce // Subject to Change
// Private Route
router.delete('/:id', adminAuth, async (req, res) => {
  try {
    const article = await Article.findById(req.params.id)
    const admin = await Admin.findById(req.admin.id)

    if (!article) {
      return res.status(404).json({ message: 'Article not found' })
    }

    if (article.admin.id.toString() !== admin._id.toString()) {
      return res.status(401).json({
        message: 'Only the admin who created the Article can delete it!',
      })
    }

    await article.deleteOne()

    res.json({ message: 'Article Deleted' })
  } catch (error) {
    console.error(error.message)
    res.status(500).send('Server Error')
  }
})

// Edit Article
// Admins Only
// Only Admin who created can Edit // Subject to change
router.put('/:id', adminAuth, async (req, res) => {
  const article = await Article.findById(req.params.id)
  const admin = await Admin.findById(req.admin.id)

  const { title, body, category, articleType } = req.body

  // Build Article Fields
  const articleFields = {}
  if (title) articleFields.title = title
  if (body) articleFields.body = body
  if (category) articleFields.category = category
  if (articleType) articleFields.articleType = articleType

  try {
    let article = await Article.findById(req.params.id)

    if (!article)
      return res.status(404).json({
        message: 'Article not found, it may have been deleted by the author',
      })

    if (article.admin.id.toString() !== req.admin.id) {
      return res.status(401).json({
        message: 'Access Denied, only the author can edit this Article',
      })
    }

    article = await Article.findByIdAndUpdate(
      req.params.id,
      { $set: articleFields },
      { new: true }
    )

    res.json(article)
  } catch (error) {
    console.error(error.message)
    res.status(500).send('Server Error')
  }
})

// USER ROUTES FOR GETTING ARTICLES
router.post('/user', async (req, res) => {
  const { id } = req.body
  try {
    let articles = await Article.find().sort({ postedAt: -1 })
    const user = await User.findById(id)
    const newArticle = []
    if (!user || user.accountStatus === 'Free' || req.body) {
      articles.forEach(article => {
        let item = {
          admin: article.admin,
          title: article.title,
          _id: article._id,
          postedAt: article.postedAt,
          articleType: article.articleType,
          body:
            article.articleType === 'Free'
              ? article.body
              : article.body.substring(0, 250),
          category: article.category,
        }
        newArticle.push(item)
      })
      res.json(newArticle)
      return
    }
    res.json(articles)
  } catch (error) {
    console.error(error.message)
    res.status(500).send('Server Error')
  }
})

// GETTING SPECIFIC CATEGORY OR TYPE
router.post('/user/selected', async (req, res) => {
  const { selected, id } = req.body
  try {
    const user = await User.findById(id)

    // Search for Articles by Category if one is selected
    let articles = await Article.find({ category: selected })
    const newArticle = []
    // Check if the category has been found
    if (articles.length > 0) {
      articles.forEach(article => {
        if (
          !user ||
          (user.accountStatus === 'Free' && article.accountType !== 'Free')
        ) {
          let item = {
            admin: article.admin,
            title: article.title,
            _id: article._id,
            postedAt: article.postedAt,
            articleType: article.articleType,
            body:
              article.articleType === 'Free'
                ? article.body
                : article.body.substring(0, 250),
            category: article.category,
          }
          newArticle.push(item)
        }
      })
      return res.json(newArticle)
      // If nothing has been found in the category Search by Article Type which is either Free or Subs Only
    } else if (!articles.length > 0) {
      articles = await Article.find({ articleType: selected })
      if (articles.length > 0) {
        articles.forEach(article => {
          if (
            !user ||
            (user.accountStatus === 'Free' && article.accountType !== 'Free')
          ) {
            let item = {
              admin: article.admin,
              title: article.title,
              _id: article._id,
              postedAt: article.postedAt,
              articleType: article.articleType,
              body:
                article.articleType === 'Free'
                  ? article.body
                  : article.body.substring(0, 250),
              category: article.category,
            }
            newArticle.push(item)
          }
        })
        return res.json(newArticle)
      }
      // If non of the above has been found, return an error
    } else if (!articles.length > 0) {
      return res.status(404).json({
        message:
          'There are no articles for your selected category. Please select another category.',
      })
    }
    return res.status(404).json({
      message:
        'There are no articles for your selected category. Please select another category.',
    })
  } catch (error) {
    console.error(error.message)
    res.status(500).send('Server Error')
  }
})

module.exports = router
