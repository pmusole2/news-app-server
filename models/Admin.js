const mongoose = require('mongoose')
const Schema = mongoose.Schema

const AdminSchema = new Schema({
  firstName: {
    type: String,
    required: true,
  },
  lastName: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  accountType: {
    type: String,
    default: 'Admin',
  },
  accountStatus: {
    type: String,
    default: 'Active',
  },
  date: {
    type: Date,
    default: Date.now(),
  },
})

module.exports = Admin = mongoose.model('admin', AdminSchema)
