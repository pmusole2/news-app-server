const mongoose = require('mongoose')
const Schema = mongoose.Schema

const ArticleSchema = new Schema({
  admin: {
    name: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
    },
    id: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'admin',
    },
  },
  title: {
    type: String,
    required: true,
  },
  body: {
    type: String,
    required: true,
  },
  category: {
    type: String,
    required: true,
  },
  articleType: {
    type: String,
    default: 'Subscribers',
  },
  postedAt: {
    type: Date,
    default: Date.now(),
  },
})

module.exports = Article = mongoose.model('article', ArticleSchema)
