const express = require('express')
const app = express()
const cors = require('cors')
const path = require('path')
const connectDB = require('./config/db')

connectDB()

app.use(cors())

app.use(express.json({ extented: false }))
app.use(express.static(path.join(__dirname, 'public')))

const PORT = process.env.PORT || 5000
app.listen(PORT, () => {
  console.log(`Server started on port ${PORT}`)
})

app.use('/api/admin', require('./routes/adminAuth'))
app.use('/api/user', require('./routes/userAuth'))
app.use('/api/articles', require('./routes/articles'))
app.use('/', (req, res) => {
  res.sendFile(path.join(__dirname + '/index.html'))
})

// const io = require('socket.io').init(server)

// io.on('connection', socket => {
//   console.log('A socket has been established')
// })
